const http = require("http");
const fs = require("fs");
const uuid4 = require('uuid').v4;


let fileReader = function (filepath){
    return new Promise((resolve,reject)=>{
        fs.readFile(filepath,"utf-8",(err,data)=>{
            if(err){
                reject(err)
            }
            else{
                resolve(data)
            }
        })
    })
}

const server = http.createServer((request,response)=>{

    let endpoint = request.url;
    let requestStatusCode = Array.isArray(endpoint.match(/^(\/GET\/)status\/([0-9]+)$/g)) ? endpoint.match(/^(\/GET\/)status\/([0-9]+)$/g).join(""):0
    let requestDelay = Array.isArray(endpoint.match(/^(\/GET\/)delay\/([0-9]+)$/g)) ? endpoint.match(/^(\/GET\/)delay\/([0-9]+)$/g).join(""):0

    switch(request.url){

        case "/GET/html":
            fileReader("./index.html")
            .then((data)=>{
                    response.writeHead("200",{
                        "Content-Type":"text/html"
                    });
                    response.write(data);
                    response.end()
            })
            .catch((err)=>{
                console.log(err);
                response.writeHead("200",{
                    "Content-Type":"text/html"
                });
                response.end()
            });
            return;

        case "/GET/json":
            fileReader("./data.json")
            .then((data)=>{
                response.writeHead("200",{
                    "Content-Type":"application/json"
                });
                response.write(data);
                response.end()
            })
            .catch((err)=>{
                console.log(err);
                response.writeHead("200",{
                    "Content-Type":"application/json"
                });
                response.end()
            });
            return;

        case "/GET/uuid":
            let uuid = uuid4();
            response.writeHead("200")
            response.write(`{ uuid : ${uuid} }`)
            response.end();
            return;

        case requestStatusCode:
            response.writeHead(requestStatusCode.match(/[0-9]/g).join(""))
            response.end()
            return;

        case requestDelay:
            let delay = Number(requestDelay.match(/[0-9]/g).join(""))
            setTimeout(()=>{
                response.writeHead("200");
                response.end()
            }, delay*1000)
            return;

        default:
            response.writeHead("404",{
                "Content-Type":"text/plain"
            })
            response.write("Page Not Found")
            response.end();
            return;
    }
})
server.listen("7000");